//
//  ComposerViewController.h
//  reveal_view_animation
//
//  Created by Mikhail Rakhmalevich on 23.12.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ComposerViewController;
@protocol ComposerViewControllerDelegate <NSObject>
- (void)composerViewController:(ComposerViewController *)controller didAddAlert:(NSString *)alert;
@end

@interface ComposerViewController : UIViewController

@property (nonatomic, weak) id<ComposerViewControllerDelegate> delegate;

@end
