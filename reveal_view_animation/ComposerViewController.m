//
//  ComposerViewController.m
//  reveal_view_animation
//
//  Created by Mikhail Rakhmalevich on 23.12.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "ComposerViewController.h"

static NSString *kPlaceholderMessage = @"Resolving this Notification requires a Resolution Note. Please enter this here...";

@interface ComposerViewController () <UITextViewDelegate>
@property (nonatomic, weak) IBOutlet UIView *backgroundView;
@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, weak) IBOutlet UITapGestureRecognizer *textViewTapRecognizer;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *addBarButtonItem;
@end

@implementation ComposerViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [_textView becomeFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [UIView animateWithDuration:0.3 animations:^{
        _backgroundView.alpha = 1.0;
    }];
}

- (void)showPlaceholder
{
    _textView.editable = NO;
    _textView.textColor = [UIColor colorWithWhite:0.0 alpha:0.5];;
    _textView.text = kPlaceholderMessage;
    _textViewTapRecognizer.enabled = YES;
}

- (void)hidePlaceholder
{
    _textView.editable = YES;
    _textView.textColor = [UIColor blackColor];
    _textView.text = @"";
    _textViewTapRecognizer.enabled = NO;
}

- (void)dismiss
{
    [_textView resignFirstResponder];
    [UIView animateWithDuration:0.3 animations:^{
        _backgroundView.alpha = 0.0;
    } completion:^(BOOL finished){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}

#pragma mark - Actions
- (IBAction)actionCancel:(UIBarButtonItem *)sender
{
    [self dismiss];
}

- (IBAction)actionAddNote:(UIBarButtonItem *)sender
{
    [_delegate composerViewController:self didAddAlert:_textView.text];
    [self dismiss];
}

- (IBAction)backgroundTapRecognized:(UITapGestureRecognizer *)tapRecognizer
{
    [_textView resignFirstResponder];
}

- (IBAction)textViewTapRecognized:(UITapGestureRecognizer *)tapRecognizer
{
    [self hidePlaceholder];
    [_textView becomeFirstResponder];
}

#pragma mark - UITextView delegate
- (void)textViewDidChange:(UITextView *)textView
{
    _addBarButtonItem.enabled = textView.text.length > 0;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView.text.length == 0) {
        [self showPlaceholder];
    }
}

@end
