//
//  HeaderView.h
//  reveal_view_animation
//
//  Created by Mikhail Rakhmalevich on 18.12.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderView : UIView

+ (CGFloat)headerHeight;

@end
