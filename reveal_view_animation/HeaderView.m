//
//  HeaderView.m
//  reveal_view_animation
//
//  Created by Mikhail Rakhmalevich on 18.12.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "HeaderView.h"

@interface HeaderView ()
@property (nonatomic, weak) IBOutlet UILabel *label;
@property (nonatomic, weak) IBOutlet UIButton *button;
@end

@implementation HeaderView

+ (CGFloat)headerHeight
{
    return 100.0f;
}

@end
