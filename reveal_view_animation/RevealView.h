//
//  RevealView.h
//  reveal_view_animation
//
//  Created by Mikhail Rakhmalevich on 17.12.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RevealView : UIView

@property (nonatomic, weak, readonly) UIView *contentView;

@end
