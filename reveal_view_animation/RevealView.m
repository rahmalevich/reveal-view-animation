//
//  RevealView.m
//  reveal_view_animation
//
//  Created by Mikhail Rakhmalevich on 17.12.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "RevealView.h"

@interface RevealView ()
@property (nonatomic, weak) IBOutlet UIView *contentView;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UILabel *textLabel;
@end

@implementation RevealView

@end
