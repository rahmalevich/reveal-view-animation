//
//  TableViewCell.h
//  reveal_view_animation
//
//  Created by Mikhail Rakhmalevich on 17.12.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "SWTableViewCell.h"

@interface RevealViewAnimationCell : SWTableViewCell

+ (CGFloat)cellHeight;

@end
