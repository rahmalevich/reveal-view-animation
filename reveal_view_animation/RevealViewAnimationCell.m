//
//  TableViewCell.m
//  reveal_view_animation
//
//  Created by Mikhail Rakhmalevich on 17.12.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "RevealViewAnimationCell.h"
#import "RevealView.h"

static CGFloat const kButtonWidth = 100.0f;

@interface SWTableViewCell (Protected)
@property (nonatomic, strong, readonly) UIScrollView *cellScrollView;
@end

@implementation RevealViewAnimationCell

#pragma mark - Initialization
+ (CGFloat)cellHeight
{
    return 100.0f;
}

- (void)awakeFromNib
{
    [self.cellScrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
    
    RevealView *rightRevealView1 = [[[NSBundle mainBundle] loadNibNamed:@"RevealView" owner:self options:nil] firstObject];
    rightRevealView1.backgroundColor = [UIColor greenColor];
    
    RevealView *rightRevealView2 = [[[NSBundle mainBundle] loadNibNamed:@"RevealView" owner:self options:nil] firstObject];
    rightRevealView2.backgroundColor = [UIColor yellowColor];
    
    [self setRightUtilityButtons:@[rightRevealView1, rightRevealView2] WithButtonWidth:kButtonWidth];
    
    RevealView *leftRevealView1 = [[[NSBundle mainBundle] loadNibNamed:@"RevealView" owner:self options:nil] firstObject];
    leftRevealView1.backgroundColor = [UIColor redColor];
    
    RevealView *leftRevealView2 = [[[NSBundle mainBundle] loadNibNamed:@"RevealView" owner:self options:nil] firstObject];
    leftRevealView2.backgroundColor = [UIColor blueColor];
    
    [self setLeftUtilityButtons:@[leftRevealView1, leftRevealView2] WithButtonWidth:kButtonWidth];
}

- (void)dealloc
{
    [self.cellScrollView removeObserver:self forKeyPath:@"contentOffset"];
}

#pragma mark - Animation
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"contentOffset"]) {
        CGFloat xOffset = [change[NSKeyValueChangeNewKey] CGPointValue].x;
        CGFloat leftButtonsWidth = self.leftUtilityButtons.count * kButtonWidth;
        
        CGFloat scale;
        NSArray *buttonsToScale;
        if (xOffset < leftButtonsWidth) { // animate left
            scale = MIN(1.0f, (leftButtonsWidth - xOffset) / kButtonWidth);
            buttonsToScale = self.leftUtilityButtons;
        } else { // animate right
            scale = MIN(1.0f, (xOffset - leftButtonsWidth) / kButtonWidth);
            buttonsToScale = self.rightUtilityButtons;
        }
        
        for (RevealView *revealView in buttonsToScale) {
            if ([revealView isKindOfClass:[RevealView class]]) {
                revealView.contentView.transform = CGAffineTransformMakeScale(scale, scale);
            }
        }
    }
}

@end
