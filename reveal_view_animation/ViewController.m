//
//  ViewController.m
//  reveal_view_animation
//
//  Created by Mikhail Rakhmalevich on 17.12.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "ViewController.h"
#import "ComposerViewController.h"
#import "RevealViewAnimationCell.h"
#import "HeaderView.h"

static NSString * const kCellReuseID = @"kCellReuseID";

@interface ViewController () <UITableViewDelegate, UITableViewDataSource, SWTableViewCellDelegate, ComposerViewControllerDelegate>
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIView *contanerView;
@end

@implementation ViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    HeaderView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"HeaderView" owner:self options:nil] firstObject];
    _tableView.tableHeaderView = headerView;
    
    [_tableView registerNib:[UINib nibWithNibName:@"RevealViewAnimationCell" bundle:nil] forCellReuseIdentifier:kCellReuseID];
    [_tableView reloadData];
    
    // add header as a subview
    HeaderView *reusableHeader = [[[NSBundle mainBundle] loadNibNamed:@"HeaderView" owner:self options:nil] firstObject];
    reusableHeader.translatesAutoresizingMaskIntoConstraints = NO;
    [_contanerView addSubview:reusableHeader];

    // setup constraints
    NSDictionary *views = NSDictionaryOfVariableBindings(reusableHeader);
    [_contanerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[reusableHeader]|" options:0 metrics:nil views:views]];
    [_contanerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[reusableHeader]|" options:0 metrics:nil views:views]];
    
    // create composer button
    UIBarButtonItem *showComposerItem = [[UIBarButtonItem alloc] initWithTitle:@"Composer" style:UIBarButtonItemStylePlain target:self action:@selector(showComposerAction:)];
    self.navigationItem.rightBarButtonItem = showComposerItem;
}

- (void)sizeHeaderToFit
{
    UIView *headerView = _tableView.tableHeaderView;
    headerView.frame = CGRectMake(0, 0, _tableView.frame.size.width, [HeaderView headerHeight]);
    _tableView.tableHeaderView = headerView;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self sizeHeaderToFit];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowComposerSegue"]) {
        ComposerViewController *composerController = segue.destinationViewController;
        composerController.delegate = self;
    }
}

#pragma mark - Actions
- (void)showComposerAction:(UIBarButtonItem *)sender
{
    [self performSegueWithIdentifier:@"ShowComposerSegue" sender:nil];
}

#pragma mark - UITableView delegate & datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [RevealViewAnimationCell cellHeight];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RevealViewAnimationCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellReuseID forIndexPath:indexPath];
    cell.delegate = self;
    return cell;
}

#pragma mark - SWTableViewCell delegate
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    [cell hideUtilityButtonsAnimated:YES];
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{
    [cell hideUtilityButtonsAnimated:YES];
}

#pragma mark - Composer Controller delegate
- (void)composerViewController:(ComposerViewController *)controller didAddAlert:(NSString *)alert
{
    NSLog(@"%@", alert);
}

@end
