//
//  main.m
//  reveal_view_animation
//
//  Created by Mikhail Rakhmalevich on 17.12.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
